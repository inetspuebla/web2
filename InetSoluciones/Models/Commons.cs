﻿using System.ComponentModel;

namespace InetSoluciones.Models
{
    public enum EnumMailType
    {
        [Description("CreateUser")]
        CreacionUsuario,
        [Description("ChangePassword")]
        CambioPassword,
        [Description("RegeneratePassword")]
        RegeneraPassword,
        [Description("UnblockUser")]
        DesbloqueaUsuario,
        [Description("Contact")]
        Contacto,
    }
}