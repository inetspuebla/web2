﻿using System;

namespace InetSoluciones.Models
{
    public class Cuentas
    {
        #region Public Properties
        public Guid IdUsuario { get; set; }
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string Contraseña { get; set; }
        public Guid CreadoPor { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaUltimoAcceso { get; set; }
        public bool Bloqueado { get; set; }
        public bool Activo { get; set; }
        public DateTime FechaUltimoCambio { get; set; }
        public int ContadorErrorAcceso { get; set; }
        public string Comentarios { get; set; }
        public bool EsAdministrador { get; set; }
        public bool EsSuperAdministrador { get; set; }
        public Guid IdEmpresa { get; set; }
        public string RFC { get; set; }
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string ColoniaLocalidad { get; set; }
        public string CiudadDelegacion { get; set; }
        public string Estado { get; set; }
        public Guid IdMembresia { get; set; }
        public Guid IdUsuarioAdmin { get; set; }
        public Guid IdRol { get; set; }
        public string Rol { get; set; }
        public bool IsCustom { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Mensaje { get; set; }
        #endregion
    }
}