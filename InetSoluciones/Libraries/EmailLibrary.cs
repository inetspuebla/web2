﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace InetSoluciones.Libraries
{
    public class EmailLibrary
    {
        private readonly SmtpClient smpt;

        public SmtpClient Smpt => smpt;

        public EmailLibrary(string hostname, int port = 25)
        {
            smpt = new SmtpClient(hostname, port);
        }

        public EmailLibrary(string hostname, int port, string username, string password, bool sslEnabled = false)
        {
            smpt = new SmtpClient(hostname, port);
            smpt.Credentials = new NetworkCredential(username, password);
            smpt.EnableSsl = sslEnabled;
        }

        public void Send(string emailFrom, string nameFrom, List<string> listTo, string subject, string plainTextBody, string htmlTextBody = null, List<string> listCC = null, List<string> listBCC = null)
        {
            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress(emailFrom, nameFrom)
            };

            if(listTo.Count <= 0)
            {
                throw new Exception("No recipients");
            }

            for(int i = 0; i < listTo.Count; i++)
            {
                mailMessage.To.Add( new MailAddress( listTo[i] ) );
            }

            if( listCC != null && listCC.Count > 0 )
            {
                for (int i = 0; i < listCC.Count; i++)
                {
                    mailMessage.CC.Add(new MailAddress(listCC[i]));
                }
            }

            if (listBCC != null && listBCC.Count > 0)
            {
                for (int i = 0; i < listBCC.Count; i++)
                {
                    mailMessage.Bcc.Add(new MailAddress(listBCC[i]));
                }
            }

            mailMessage.Subject = subject;

            if(htmlTextBody != null)
            {
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = htmlTextBody;
            }
            else
            {
                mailMessage.IsBodyHtml = false;
                mailMessage.Body = plainTextBody;

                AlternateView alternateView = AlternateView.CreateAlternateViewFromString(plainTextBody, null, "text/plain");
                alternateView.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                mailMessage.AlternateViews.Add(alternateView);
            }

            smpt.Send(mailMessage);
        }
    }
}