﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InetSoluciones.Helpers
{
    public class ResponseWrapper
    {
        #region Propiedades
        public int Estatus { get; set; }

        public string Mensaje { get; set; }

        public List<string> Errores { get; }
        #endregion

        public ResponseWrapper()
        {
            Errores = new List<string>(0);
        }

        #region Métodos
        public void AnadirError(string error)
        {
            Errores.Add(error);
        }
        #endregion
    }
}