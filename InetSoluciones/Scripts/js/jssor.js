﻿(function (h, g, c, j, d, l, k) { /*! Jssor */
    new (function () { });
    var e = {
        ud: function (a) { return -c.cos(a * c.PI) / 2 + .5 }, qd: function (a) { return a }, ze: function (a) { return a * a }, vd: function (a) { return -a * (a - 2) }, ve: function (a) { return (a *= 2) < 1 ? 1 / 2 * a * a : -1 / 2 * (--a * (a - 2) - 1) }, ue: function (a) { return a * a * a }, te: function (a) { return (a -= 1) * a * a + 1 }, se: function (a) { return (a *= 2) < 1 ? 1 / 2 * a * a * a : 1 / 2 * ((a -= 2) * a * a + 2) }, qe: function (a) { return a * a * a * a }, pe: function (a) { return -((a -= 1) * a * a * a - 1) }, oe: function (a) { return (a *= 2) < 1 ? 1 / 2 * a * a * a * a : -1 / 2 * ((a -= 2) * a * a * a - 2) }, ne: function (a) { return a * a * a * a * a }, me: function (a) { return (a -= 1) * a * a * a * a + 1 }, le: function (a) { return (a *= 2) < 1 ? 1 / 2 * a * a * a * a * a : 1 / 2 * ((a -= 2) * a * a * a * a + 2) }, ke: function (a) { return 1 - c.cos(c.PI / 2 * a) }, je: function (a) { return c.sin(c.PI / 2 * a) }, Ae: function (a) { return -1 / 2 * (c.cos(c.PI * a) - 1) }, Be: function (a) { return a == 0 ? 0 : c.pow(2, 10 * (a - 1)) }, Ce: function (a) { return a == 1 ? 1 : -c.pow(2, -10 * a) + 1 }, De: function (a) { return a == 0 || a == 1 ? a : (a *= 2) < 1 ? 1 / 2 * c.pow(2, 10 * (a - 1)) : 1 / 2 * (-c.pow(2, -10 * --a) + 2) }, Ve: function (a) { return -(c.sqrt(1 - a * a) - 1) }, Ue: function (a) { return c.sqrt(1 - (a -= 1) * a) }, Te: function (a) { return (a *= 2) < 1 ? -1 / 2 * (c.sqrt(1 - a * a) - 1) : 1 / 2 * (c.sqrt(1 - (a -= 2) * a) + 1) },
        Pe: function (a) {
            if (!a || a == 1) return a;
            var b = .3, d = .075;
            return -(c.pow(2, 10 * (a -= 1)) * c.sin((a - d) * 2 * c.PI / b))
        },
        Oe: function (a) {
            if (!a || a == 1) return a;
            var b = .3, d = .075;
            return c.pow(2, -10 * a) * c.sin((a - d) * 2 * c.PI / b) + 1
        },
        We: function (a) {
            if (!a || a == 1) return a;
            var b = .45, d = .1125;
            return (a *= 2) < 1 ? -.5 * c.pow(2, 10 * (a -= 1)) * c.sin((a - d) * 2 * c.PI / b) : c.pow(2, -10 * (a -= 1)) * c.sin((a - d) * 2 * c.PI / b) * .5 + 1
        },
        Ne: function (a) {
            var b = 1.70158;
            return a * a * ((b + 1) * a - b)
        },
        Ie: function (a) {
            var b = 1.70158;
            return (a -= 1) * a * ((b + 1) * a + b) + 1
        },
        He: function (a) {
            var b = 1.70158;
            return (a *= 2) < 1 ? 1 / 2 * a * a * (((b *= 1.525) + 1) * a - b) : 1 / 2 * ((a -= 2) * a * (((b *= 1.525) + 1) * a + b) + 2)
        },
        ad: function (a) { return 1 - e.lc(1 - a) },
        lc: function (a) { return a < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : a < 2.5 / 2.75 ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375 },
        ie: function (a) { return a < 1 / 2 ? e.ad(a * 2) * .5 : e.lc(a * 2 - 1) * .5 + .5 },
        Me: function () { return 1 - c.abs(1) },
        Zd: function (a) { return 1 - c.cos(a * c.PI * 2) },
        Kd: function (a) { return c.sin(a * c.PI * 2) },
        Ed: function (a) { return 1 - ((a *= 2) < 1 ? (a = 1 - a) * a * a : (a -= 1) * a * a) },
        Ld: function (a) { return (a *= 2) < 1 ? a * a * a : (a = 2 - a) * a * a }
    },
        f = { Ad: e.ud, zd: e.qd, xd: e.ze, Dd: e.vd, Od: e.ve, Nd: e.ue, be: e.te, ge: e.se, fe: e.qe, Vd: e.pe, Ud: e.oe, Td: e.ne, ld: e.me, Sd: e.le, Qd: e.ke, Pd: e.je, ee: e.Ae, ae: e.Be, Md: e.Ce, Cd: e.De, Fd: e.Ve, Jd: e.Ue, Xe: e.Te, ag: e.Pe, Zf: e.Oe, Xf: e.We, Wf: e.Ne, Vf: e.Ie, Ye: e.He, Uf: e.ad, Rf: e.lc, Pf: e.ie, Mf: e.Me, Lf: e.Zd, Sf: e.Kd, cg: e.Ed, eg: e.Ld };
    var b = new function () {
        var f = this, Bb = /\S+/g, G = 1, db = 2, hb = 3, gb = 4, lb = 5, H, r = 0, i = 0, s = 0, W = 0, z = 0, J = navigator, pb = J.appName, o = J.userAgent, p = parseFloat;

        function zb() {
            if (!H) {
                H = { tf: "ontouchstart" in h || "createTouch" in g };
                var a;
                if (J.pointerEnabled || (a = J.msPointerEnabled)) H.jd = a ? "msTouchAction" : "touchAction"
            }
            return H
        }

        function v(j) {
            if (!r) {
                r = -1;
                if (pb == "Microsoft Internet Explorer" && !!h.attachEvent && !!h.ActiveXObject) {
                    var e = o.indexOf("MSIE");
                    r = G;
                    s = p(o.substring(e + 5, o.indexOf(";", e))); /*@cc_on W=@_jscript_version@*/;
                    i = g.documentMode || s
                } else if (pb == "Netscape" && !!h.addEventListener) {
                    var d = o.indexOf("Firefox"), b = o.indexOf("Safari"), f = o.indexOf("Chrome"), c = o.indexOf("AppleWebKit");
                    if (d >= 0) {
                        r = db;
                        i = p(o.substring(d + 8))
                    } else if (b >= 0) {
                        var k = o.substring(0, b).lastIndexOf("/");
                        r = f >= 0 ? gb : hb;
                        i = p(o.substring(k + 1, b))
                    } else {
                        var a = /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/i.exec(o);
                        if (a) {
                            r = G;
                            i = s = p(a[1])
                        }
                    }
                    if (c >= 0) z = p(o.substring(c + 12))
                } else {
                    var a = /(opera)(?:.*version|)[ \/]([\w.]+)/i.exec(o);
                    if (a) {
                        r = lb;
                        i = p(a[2])
                    }
                }
            }
            return j == r
        }

        function q() { return v(G) }

        function R() { return q() && (i < 6 || g.compatMode == "BackCompat") }

        function fb() { return v(hb) }

        function kb() { return v(lb) }

        function wb() { return fb() && z > 534 && z < 535 }

        function K() {
            v();
            return z > 537 || i > 42 || r == G && i >= 11
        }

        function P() { return q() && i < 9 }

        function xb(a) {
            var b, c;
            return function (f) {
                if (!b) {
                    b = d;
                    var e = a.substr(0, 1).toUpperCase() + a.substr(1);
                    n([a].concat(["WebKit", "ms", "Moz", "O", "webkit"]), function (g, d) {
                        var b = a;
                        if (d) b = g + e;
                        if (f.style[b] != k) return c = b
                    })
                }
                return c
            }
        }

        function vb(b) {
            var a;
            return function (c) {
                a = a || xb(b)(c) || b;
                return a
            }
        }

        var L = vb("transform");

        function ob(a) { return {}.toString.call(a) }

        var F;

        function Hb() {
            if (!F) {
                F = {};
                n(["Boolean", "Number", "String", "Function", "Array", "Date", "RegExp", "Object"], function (a) { F["[object " + a + "]"] = a.toLowerCase() })
            }
            return F
        }

        function n(b, d) {
            var a, c;
            if (ob(b) == "[object Array]") {
                for (a = 0; a < b.length; a++) if (c = d(b[a], a, b)) return c
            } else for (a in b) if (c = d(b[a], a, b)) return c
        }

        function C(a) { return a == j ? String(a) : Hb()[ob(a)] || "object" }

        function mb(a) { for (var b in a) return d }

        function A(a) {
            try {
                return C(a) == "object" && !a.nodeType && a != a.window && (!a.constructor || {}.hasOwnProperty.call(a.constructor.prototype, "isPrototypeOf"))
            } catch (b) {
            }
        }

        function u(a, b) { return { x: a, y: b } }

        function sb(b, a) { setTimeout(b, a || 0) }

        function I(b, d, c) {
            var a = !b || b == "inherit" ? "" : b;
            n(d, function (c) {
                var b = c.exec(a);
                if (b) {
                    var d = a.substr(0, b.index), e = a.substr(b.index + b[0].length + 1, a.length - 1);
                    a = d + e
                }
            });
            a = c + (!a.indexOf(" ") ? "" : " ") + a;
            return a
        }

        function ub(b, a) { if (i < 9) b.style.filter = a }

        f.Hf = zb;
        f.kd = q;
        f.Df = fb;
        f.vf = K;
        f.od = P;
        xb("transform");
        f.sd = function () { return i };
        f.rd = sb;

        function Z(a) { a.constructor === Z.caller && a.pd && a.pd.apply(a, Z.caller.arguments) }

        f.pd = Z;
        f.qb = function (a) {
            if (f.uf(a)) a = g.getElementById(a);
            return a
        };

        function t(a) { return a || h.event }

        f.td = t;
        f.Zb = function (b) {
            b = t(b);
            var a = b.target || b.srcElement || g;
            if (a.nodeType == 3) a = f.md(a);
            return a
        };
        f.Ec = function (a) {
            a = t(a);
            return { x: a.pageX || a.clientX || 0, y: a.pageY || a.clientY || 0 }
        };

        function D(c, d, a) {
            if (a !== k) c.style[d] = a == k ? "" : a;
            else {
                var b = c.currentStyle || c.style;
                a = b[d];
                if (a == "" && h.getComputedStyle) {
                    b = c.ownerDocument.defaultView.getComputedStyle(c, j);
                    b && (a = b.getPropertyValue(d) || b[d])
                }
                return a
            }
        }

        function bb(b, c, a, d) {
            if (a !== k) {
                if (a == j) a = "";
                else d && (a += "px");
                D(b, c, a)
            } else return p(D(b, c))
        }

        function m(c, a) {
            var d = a ? bb : D, b;
            if (a & 4) b = vb(c);
            return function (e, f) { return d(e, b ? b(e) : c, f, a & 2) }
        }

        function Eb(b) {
            if (q() && s < 9) {
                var a = /opacity=([^)]*)/.exec(b.style.filter || "");
                return a ? p(a[1]) / 100 : 1
            } else return p(b.style.opacity || "1")
        }

        function Gb(b, a, f) {
            if (q() && s < 9) {
                var h = b.style.filter || "", i = new RegExp(/[\s]*alpha\([^\)]*\)/g), e = c.round(100 * a), d = "";
                if (e < 100 || f) d = "alpha(opacity=" + e + ") ";
                var g = I(h, [i], d);
                ub(b, g)
            } else b.style.opacity = a == 1 ? "" : c.round(a * 100) / 100
        }

        var M = { ab: ["rotate"], R: ["rotateX"], S: ["rotateY"], Ab: ["skewX"], Bb: ["skewY"] };
        if (!K()) M = B(M, { p: ["scaleX", 2], n: ["scaleY", 2], Q: ["translateZ", 1] });

        function N(d, a) {
            var c = "";
            if (a) {
                if (q() && i && i < 10) {
                    delete a.R;
                    delete a.S;
                    delete a.Q
                }
                b.f(a, function (d, b) {
                    var a = M[b];
                    if (a) {
                        var e = a[1] || 0;
                        if (O[b] != d) c += " " + a[0] + "(" + d + (["deg", "px", ""])[e] + ")"
                    }
                });
                if (K()) {
                    if (a.fb || a.bb || a.Q) c += " translate3d(" + (a.fb || 0) + "px," + (a.bb || 0) + "px," + (a.Q || 0) + "px)";
                    if (a.p == k) a.p = 1;
                    if (a.n == k) a.n = 1;
                    if (a.p != 1 || a.n != 1) c += " scale3d(" + a.p + ", " + a.n + ", 1)"
                }
            }
            d.style[L(d)] = c
        }

        f.uc = m("transformOrigin", 4);
        f.xf = m("backfaceVisibility", 4);
        f.yf = m("transformStyle", 4);
        f.zf = m("perspective", 6);
        f.Af = m("perspectiveOrigin", 4);
        f.Bf = function (a, b) {
            if (q() && s < 9 || s < 10 && R()) a.style.zoom = b == 1 ? "" : b;
            else {
                var c = L(a), f = "scale(" + b + ")", e = a.style[c], g = new RegExp(/[\s]*scale\(.*?\)/g), d = I(e, [g], f);
                a.style[c] = d
            }
        };
        f.Wb = function (b, a) {
            return function (c) {
                c = t(c);
                var e = c.type, d = c.relatedTarget || (e == "mouseout" ? c.toElement : c.fromElement);
                (!d || d !== a && !f.Cf(a, d)) && b(c)
            }
        };
        f.a = function (a, d, b, c) {
            a = f.qb(a);
            if (a.addEventListener) {
                d == "mousewheel" && a.addEventListener("DOMMouseScroll", b, c);
                a.addEventListener(d, b, c)
            } else if (a.attachEvent) {
                a.attachEvent("on" + d, b);
                c && a.setCapture && a.setCapture()
            }
        };
        f.J = function (a, c, d, b) {
            a = f.qb(a);
            if (a.removeEventListener) {
                c == "mousewheel" && a.removeEventListener("DOMMouseScroll", d, b);
                a.removeEventListener(c, d, b)
            } else if (a.detachEvent) {
                a.detachEvent("on" + c, d);
                b && a.releaseCapture && a.releaseCapture()
            }
        };
        f.Jb = function (a) {
            a = t(a);
            a.preventDefault && a.preventDefault();
            a.cancel = d;
            a.returnValue = l
        };
        f.Gf = function (a) {
            a = t(a);
            a.stopPropagation && a.stopPropagation();
            a.cancelBubble = d
        };
        f.K = function (d, c) {
            var a = [].slice.call(arguments, 2),
                b = function () {
                    var b = a.concat([].slice.call(arguments, 0));
                    return c.apply(d, b)
                };
            return b
        };
        f.Lc = function (a, b) {
            if (b == k) return a.textContent || a.innerText;
            var c = g.createTextNode(b);
            f.Sb(a);
            a.appendChild(c)
        };
        f.xb = function (d, c) {
            for (var b = [], a = d.firstChild; a; a = a.nextSibling) (c || a.nodeType == 1) && b.push(a);
            return b
        };

        function nb(a, c, e, b) {
            b = b || "u";
            for (a = a ? a.firstChild : j; a; a = a.nextSibling)
                if (a.nodeType == 1) {
                    if (V(a, b) == c) return a;
                    if (!e) {
                        var d = nb(a, c, e, b);
                        if (d) return d
                    }
                }
        }

        f.q = nb;

        function T(a, d, f, b) {
            b = b || "u";
            var c = [];
            for (a = a ? a.firstChild : j; a; a = a.nextSibling)
                if (a.nodeType == 1) {
                    V(a, b) == d && c.push(a);
                    if (!f) {
                        var e = T(a, d, f, b);
                        if (e.length) c = c.concat(e)
                    }
                }
            return c
        }

        f.df = T;

        function ib(a, c, d) {
            for (a = a ? a.firstChild : j; a; a = a.nextSibling)
                if (a.nodeType == 1) {
                    if (a.tagName == c) return a;
                    if (!d) {
                        var b = ib(a, c, d);
                        if (b) return b
                    }
                }
        }

        f.ef = ib;
        f.gf = function (b, a) { return b.getElementsByTagName(a) };

        function B() {
            var e = arguments, d, c, b, a, g = 1 & e[0], f = 1 + g;
            d = e[f - 1] || {};
            for (; f < e.length; f++)
                if (c = e[f])
                    for (b in c) {
                        a = c[b];
                        if (a !== k) {
                            a = c[b];
                            var h = d[b];
                            d[b] = g && (A(h) || A(a)) ? B(g, {}, h, a) : a
                        }
                    }
            return d
        }

        f.M = B;

        function ab(f, g) {
            var d = {}, c, a, b;
            for (c in f) {
                a = f[c];
                b = g[c];
                if (a !== b) {
                    var e;
                    if (A(a) && A(b)) {
                        a = ab(a, b);
                        e = !mb(a)
                    }
                    !e && (d[c] = a)
                }
            }
            return d
        }

        f.Xc = function (a) { return C(a) == "function" };
        f.uf = function (a) { return C(a) == "string" };
        f.Yb = function (a) { return !isNaN(p(a)) && isFinite(a) };
        f.f = n;
        f.Kf = A;

        function S(a) { return g.createElement(a) }

        f.wb = function () { return S("DIV") };
        f.mf = function () { return S("SPAN") };
        f.Pc = function () { };

        function X(b, c, a) {
            if (a == k) return b.getAttribute(c);
            b.setAttribute(c, a)
        }

        function V(a, b) { return X(a, b) || X(a, "data-" + b) }

        f.v = X;
        f.j = V;

        function x(b, a) {
            if (a == k) return b.className;
            b.className = a
        }

        f.Kc = x;

        function rb(b) {
            var a = {};
            n(b, function (b) { a[b] = b });
            return a
        }

        function tb(b, a) { return b.match(a || Bb) }

        function Q(b, a) { return rb(tb(b || "", a)) }

        f.Of = tb;

        function cb(b, c) {
            var a = "";
            n(c, function (c) {
                a && (a += b);
                a += c
            });
            return a
        }

        function E(a, c, b) { x(a, cb(" ", B(ab(Q(x(a)), Q(c)), Q(b)))) }

        f.md = function (a) { return a.parentNode };
        f.T = function (a) { f.U(a, "none") };
        f.O = function (a, b) { f.U(a, b ? "none" : "") };
        f.jg = function (b, a) { b.removeAttribute(a) };
        f.og = function () { return q() && i < 10 };
        f.pg = function (d, a) {
            if (a) d.style.clip = "rect(" + c.round(a.g || a.B || 0) + "px " + c.round(a.z) + "px " + c.round(a.D) + "px " + c.round(a.i || a.F || 0) + "px)";
            else if (a !== k) {
                var g = d.style.cssText, f = [new RegExp(/[\s]*clip: rect\(.*?\)[;]?/i), new RegExp(/[\s]*cliptop: .*?[;]?/i), new RegExp(/[\s]*clipright: .*?[;]?/i), new RegExp(/[\s]*clipbottom: .*?[;]?/i), new RegExp(/[\s]*clipleft: .*?[;]?/i)], e = I(g, f, "");
                b.sb(d, e)
            }
        };
        f.P = function () { return +new Date };
        f.I = function (b, a) { b.appendChild(a) };
        f.Db = function (b, a, c) { (c || a.parentNode).insertBefore(b, a) };
        f.Cb = function (b, a) {
            a = a || b.parentNode;
            a && a.removeChild(b)
        };
        f.Tf = function (a, b) { n(a, function (a) { f.Cb(a, b) }) };
        f.Sb = function (a) { f.Tf(f.xb(a, d), a) };
        f.bg = function (a, b) {
            var c = f.md(a);
            b & 1 && f.E(a, (f.l(c) - f.l(a)) / 2);
            b & 2 && f.C(a, (f.m(c) - f.m(a)) / 2)
        };
        f.qc = function (b, a) { return parseInt(b, a || 10) };
        f.Yf = p;
        f.Cf = function (b, a) {
            var c = g.body;
            while (a && b !== a && c !== a)
                try {
                    a = a.parentNode
                } catch (d) {
                    return l
                }
            return b === a
        };

        function Y(d, c, b) {
            var a = d.cloneNode(!c);
            !b && f.jg(a, "id");
            return a
        }

        f.pb = Y;
        f.gb = function (e, g) {
            var a = new Image;

            function b(e, d) {
                f.J(a, "load", b);
                f.J(a, "abort", c);
                f.J(a, "error", c);
                g && g(a, d)
            }

            function c(a) { b(a, d) }

            if (kb() && i < 11.6 || !e) b(!e);
            else {
                f.a(a, "load", b);
                f.a(a, "abort", c);
                f.a(a, "error", c);
                a.src = e
            }
        };
        f.Ze = function (d, a, e) {
            var c = d.length + 1;

            function b(b) {
                c--;
                if (a && b && b.src == a.src) a = b;
                !c && e && e(a)
            }

            n(d, function (a) { f.gb(a.src, b) });
            b()
        };
        f.lf = function (a, g, i, h) {
            if (h) a = Y(a);
            var c = T(a, g);
            if (!c.length) c = b.gf(a, g);
            for (var f = c.length - 1; f > -1; f--) {
                var d = c[f], e = Y(i);
                x(e, x(d));
                b.sb(e, d.style.cssText);
                b.Db(e, d);
                b.Cb(d)
            }
            return a
        };

        function Ib(a) {
            var l = this, p = "", r = ["av", "pv", "ds", "dn"], e = [], q, j = 0, h = 0, d = 0;

            function i() {
                E(a, q, e[d || j || h & 2 || h]);
                b.L(a, "pointer-events", d ? "none" : "")
            }

            function c() {
                j = 0;
                i();
                f.J(g, "mouseup", c);
                f.J(g, "touchend", c);
                f.J(g, "touchcancel", c)
            }

            function o(a) {
                if (d) f.Jb(a);
                else {
                    j = 4;
                    i();
                    f.a(g, "mouseup", c);
                    f.a(g, "touchend", c);
                    f.a(g, "touchcancel", c)
                }
            }

            l.Ge = function (a) {
                if (a === k) return h;
                h = a & 2 || a & 1;
                i()
            };
            l.Yc = function (a) {
                if (a === k) return !d;
                d = a ? 0 : 3;
                i()
            };
            l.H = a = f.qb(a);
            var m = b.Of(x(a));
            if (m) p = m.shift();
            n(r, function (a) { e.push(p + a) });
            q = cb(" ", e);
            e.unshift("");
            f.a(a, "mousedown", o);
            f.a(a, "touchstart", o)
        }

        f.rc = function (a) { return new Ib(a) };
        f.L = D;
        f.Hb = m("overflow");
        f.C = m("top", 2);
        f.E = m("left", 2);
        f.l = m("width", 2);
        f.m = m("height", 2);
        f.wd = m("marginLeft", 2);
        f.he = m("marginTop", 2);
        f.A = m("position");
        f.U = m("display");
        f.s = m("zIndex", 1);
        f.oc = function (b, a, c) {
            if (a != k) Gb(b, a, c);
            else return Eb(b)
        };
        f.sb = function (a, b) {
            if (b != k) a.style.cssText = b;
            else return a.style.cssText
        };
        var U = { ib: f.oc, g: f.C, i: f.E, jb: f.l, kb: f.m, lb: f.A, Cg: f.U, mb: f.s };

        function w(g, l) {
            var e = P(), b = K(), d = wb(), h = L(g);

            function i(b, d, a) {
                var e = b.Y(u(-d / 2, -a / 2)), f = b.Y(u(d / 2, -a / 2)), g = b.Y(u(d / 2, a / 2)), h = b.Y(u(-d / 2, a / 2));
                b.Y(u(300, 300));
                return u(c.min(e.x, f.x, g.x, h.x) + d / 2, c.min(e.y, f.y, g.y, h.y) + a / 2)
            }

            function a(d, a) {
                a = a || {};
                var g = a.Q || 0, l = (a.R || 0) % 360, m = (a.S || 0) % 360, o = (a.ab || 0) % 360, p = a.Ag;
                if (e) {
                    g = 0;
                    l = 0;
                    m = 0;
                    p = 0
                }
                var c = new Db(a.fb, a.bb, g);
                c.R(l);
                c.S(m);
                c.ce(o);
                c.de(a.Ab, a.Bb);
                c.nc(a.p, a.n, p);
                if (b) {
                    c.yb(a.F, a.B);
                    d.style[h] = c.Wd()
                } else if (!W || W < 9) {
                    var j = "";
                    if (o || a.p != k && a.p != 1 || a.n != k && a.n != 1) {
                        var n = i(c, a.W, a.X);
                        f.he(d, n.y);
                        f.wd(d, n.x);
                        j = c.yd()
                    }
                    var r = d.style.filter, s = new RegExp(/[\s]*progid:DXImageTransform\.Microsoft\.Matrix\([^\)]*\)/g), q = I(r, [s], j);
                    ub(d, q)
                }
            }

            w = function (e, c) {
                c = c || {};
                var h = c.F, i = c.B, g;
                n(U, function (a, b) {
                    g = c[b];
                    g !== k && a(e, g)
                });
                f.pg(e, c.c);
                if (!b) {
                    h != k && f.E(e, c.Vc + h);
                    i != k && f.C(e, c.Uc + i)
                }
                if (c.Bd)
                    if (d) sb(f.K(j, N, e, c));
                    else a(e, c)
            };
            f.Pb = N;
            if (d) f.Pb = w;
            if (e) f.Pb = a;
            else if (!b) a = N;
            f.N = w;
            w(g, l)
        }

        f.Pb = w;
        f.N = w;

        function Db(i, l, p) {
            var d = this, b = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, i || 0, l || 0, p || 0, 1], h = c.sin, g = c.cos, m = c.tan;

            function f(a) { return a * c.PI / 180 }

            function o(a, b) { return { x: a, y: b } }

            function n(c, e, l, m, o, r, t, u, w, z, A, C, E, b, f, k, a, g, i, n, p, q, s, v, x, y, B, D, F, d, h, j) { return [c * a + e * p + l * x + m * F, c * g + e * q + l * y + m * d, c * i + e * s + l * B + m * h, c * n + e * v + l * D + m * j, o * a + r * p + t * x + u * F, o * g + r * q + t * y + u * d, o * i + r * s + t * B + u * h, o * n + r * v + t * D + u * j, w * a + z * p + A * x + C * F, w * g + z * q + A * y + C * d, w * i + z * s + A * B + C * h, w * n + z * v + A * D + C * j, E * a + b * p + f * x + k * F, E * g + b * q + f * y + k * d, E * i + b * s + f * B + k * h, E * n + b * v + f * D + k * j] }

            function e(c, a) { return n.apply(j, (a || b).concat(c)) }

            d.nc = function (a, c, d) {
                if (a == k) a = 1;
                if (c == k) c = 1;
                if (d == k) d = 1;
                if (a != 1 || c != 1 || d != 1) b = e([a, 0, 0, 0, 0, c, 0, 0, 0, 0, d, 0, 0, 0, 0, 1])
            };
            d.yb = function (a, c, d) {
                b[12] += a || 0;
                b[13] += c || 0;
                b[14] += d || 0
            };
            d.R = function (c) {
                if (c) {
                    a = f(c);
                    var d = g(a), i = h(a);
                    b = e([1, 0, 0, 0, 0, d, i, 0, 0, -i, d, 0, 0, 0, 0, 1])
                }
            };
            d.S = function (c) {
                if (c) {
                    a = f(c);
                    var d = g(a), i = h(a);
                    b = e([d, 0, -i, 0, 0, 1, 0, 0, i, 0, d, 0, 0, 0, 0, 1])
                }
            };
            d.ce = function (c) {
                if (c) {
                    a = f(c);
                    var d = g(a), i = h(a);
                    b = e([d, i, 0, 0, -i, d, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1])
                }
            };
            d.de = function (a, c) {
                if (a || c) {
                    i = f(a);
                    l = f(c);
                    b = e([1, m(l), 0, 0, m(i), 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1])
                }
            };
            d.Y = function (c) {
                var a = e(b, [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, c.x, c.y, 0, 1]);
                return o(a[12], a[13])
            };
            d.Wd = function () { return "matrix3d(" + b.join(",") + ")" };
            d.yd = function () { return "progid:DXImageTransform.Microsoft.Matrix(M11=" + b[0] + ", M12=" + b[4] + ", M21=" + b[1] + ", M22=" + b[5] + ", SizingMethod='auto expand')" }
        }

        new function () {
            var a = this;

            function b(d, g) {
                for (var j = d[0].length, i = d.length, h = g[0].length, f = [], c = 0; c < i; c++)
                    for (var k = f[c] = [], b = 0; b < h; b++) {
                        for (var e = 0, a = 0; a < j; a++) e += d[c][a] * g[a][b];
                        k[b] = e
                    }
                return f
            }

            a.p = function (b, c) { return a.Tc(b, c, 0) };
            a.n = function (b, c) { return a.Tc(b, 0, c) };
            a.Tc = function (a, c, d) { return b(a, [[c, 0], [0, d]]) };
            a.Y = function (d, c) {
                var a = b(d, [[c.x], [c.y]]);
                return u(a[0][0], a[1][0])
            }
        };
        var O = { Vc: 0, Uc: 0, F: 0, B: 0, Z: 1, p: 1, n: 1, ab: 0, R: 0, S: 0, fb: 0, bb: 0, Q: 0, Ab: 0, Bb: 0 };
        f.Sc = function (a) {
            var c = a || {};
            if (a)
                if (b.Xc(a)) c = { mc: c };
                else if (b.Xc(a.c)) c.c = { mc: a.c };
            return c
        };

        function qb(c, a) {
            var b = {};
            n(c, function (c, d) {
                var e = c;
                if (a[d] != k)
                    if (f.Yb(c)) e = c + a[d];
                    else e = qb(c, a[d]);
                b[d] = e
            });
            return b
        }

        f.Ee = qb;
        f.Fe = function (l, m, w, n, y, z, o) {
            var a = m;
            if (l) {
                a = {};
                for (var g in m) {
                    var A = z[g] || 1, v = y[g] || [0, 1], f = (w - v[0]) / v[1];
                    f = c.min(c.max(f, 0), 1);
                    f = f * A;
                    var u = c.floor(f);
                    if (f != u) f -= u;
                    var h = n.mc || e.ud, i, B = l[g], q = m[g];
                    if (b.Yb(q)) {
                        h = n[g] || h;
                        var x = h(f);
                        i = B + q * x
                    } else {
                        i = b.M({ Gb: {} }, l[g]);
                        b.f(q.Gb || q, function (d, a) {
                            if (n.c) h = n.c[a] || n.c.mc || h;
                            var c = h(f), b = d * c;
                            i.Gb[a] = b;
                            i[a] += b
                        })
                    }
                    a[g] = i
                }
                var t = b.f(m, function (b, a) { return O[a] != k });
                t && b.f(O, function (c, b) { if (a[b] == k && l[b] !== k) a[b] = l[b] });
                if (t) {
                    if (a.Z) a.p = a.n = a.Z;
                    a.W = o.W;
                    a.X = o.X;
                    a.Bd = d
                }
            }
            if (m.c && o.yb) {
                var p = a.c.Gb, s = (p.g || 0) + (p.D || 0), r = (p.i || 0) + (p.z || 0);
                a.i = (a.i || 0) + r;
                a.g = (a.g || 0) + s;
                a.c.i -= r;
                a.c.z -= r;
                a.c.g -= s;
                a.c.D -= s
            }
            if (a.c && b.og() && !a.c.g && !a.c.i && a.c.z == o.W && a.c.D == o.X) a.c = j;
            return a
        }
    };

    function n() {
        var a = this, d = [];

        function i(a, b) { d.push({ kc: a, pc: b }) }

        function g(a, c) { b.f(d, function (b, e) { b.kc == a && b.pc === c && d.splice(e, 1) }) }

        a.ub = a.addEventListener = i;
        a.removeEventListener = g;
        a.k = function (a) {
            var c = [].slice.call(arguments, 1);
            b.f(d, function (b) { b.kc == a && b.pc.apply(h, c) })
        }
    }

    var m = function (z, C, i, J, M, L) {
        z = z || 0;
        var a = this, q, n, o, u, A = 0, G, H, F, B, y = 0, g = 0, m = 0, D, k, f, e, p, w = [], x;

        function O(a) {
            f += a;
            e += a;
            k += a;
            g += a;
            m += a;
            y += a
        }

        function t(o) {
            var h = o;
            if (p && (h >= e || h <= f)) h = ((h - f) % p + p) % p + f;
            if (!D || u || g != h) {
                var j = c.min(h, e);
                j = c.max(j, f);
                if (!D || u || j != m) {
                    if (L) {
                        var l = (j - k) / (C || 1);
                        if (i.Se) l = 1 - l;
                        var n = b.Fe(M, L, l, G, F, H, i);
                        if (x) b.f(n, function (b, a) { x[a] && x[a](J, b) });
                        else b.N(J, n)
                    }
                    a.hc(m - k, j - k);
                    m = j;
                    b.f(w, function (b, c) {
                        var a = o < g ? w[w.length - c - 1] : b;
                        a.u(m - y)
                    });
                    var r = g, q = m;
                    g = h;
                    D = d;
                    a.Nb(r, q)
                }
            }
        }

        function E(a, b, d) {
            b && a.Mb(e);
            if (!d) {
                f = c.min(f, a.gc() + y);
                e = c.max(e, a.Kb() + y)
            }
            w.push(a)
        }

        var r = h.requestAnimationFrame || h.webkitRequestAnimationFrame || h.mozRequestAnimationFrame || h.msRequestAnimationFrame;
        if (b.Df() && b.sd() < 7) r = j;
        r = r || function (a) { b.rd(a, i.Jc) };

        function I() {
            if (q) {
                var d = b.P(), e = c.min(d - A, i.Ic), a = g + e * o;
                A = d;
                if (a * o >= n * o) a = n;
                t(a);
                if (!u && a * o >= n * o) K(B);
                else r(I)
            }
        }

        function s(h, i, j) {
            if (!q) {
                q = d;
                u = j;
                B = i;
                h = c.max(h, f);
                h = c.min(h, e);
                n = h;
                o = n < g ? -1 : 1;
                a.Hc();
                A = b.P();
                r(I)
            }
        }

        function K(b) {
            if (q) {
                u = q = B = l;
                a.Gc();
                b && b()
            }
        }

        a.vc = function (a, b, c) { s(a ? g + a : e, b, c) };
        a.wc = s;
        a.db = K;
        a.re = function (a) { s(a) };
        a.V = function () { return g };
        a.xc = function () { return n };
        a.nb = function () { return m };
        a.u = t;
        a.yb = function (a) { t(g + a) };
        a.zc = function () { return q };
        a.ye = function (a) { p = a };
        a.Mb = O;
        a.zb = function (a, b) { E(a, 0, b) };
        a.fc = function (a) { E(a, 1) };
        a.dg = function (a) { e += a };
        a.gc = function () { return f };
        a.Kb = function () { return e };
        a.Nb = a.Hc = a.Gc = a.hc = b.Pc;
        a.ec = b.P();
        i = b.M({ Jc: 16, Ic: 50 }, i);
        p = i.Dc;
        x = i.tg;
        f = k = z;
        e = z + C;
        H = i.qg || {};
        F = i.ng || {};
        G = b.Sc(i.eb)
    };
    new (function () { });
    var i = function (p, fc) {
        var f = this;

        function Bc() {
            var a = this;
            m.call(a, -1e8, 2e8);
            a.sf = function () {
                var b = a.nb(), d = c.floor(b), f = t(d), e = b - c.floor(b);
                return { cb: f, bf: d, lb: e }
            };
            a.Nb = function (b, a) {
                var e = c.floor(a);
                if (e != a && a > b) e++;
                Ub(e, d);
                f.k(i.cf, t(a), t(b), a, b)
            }
        }

        function Ac() {
            var a = this;
            m.call(a, 0, 0, { Dc: r });
            b.f(C, function (b) {
                D & 1 && b.ye(r);
                a.fc(b);
                b.Mb(ib / bc)
            })
        }

        function zc() {
            var a = this, b = Tb.H;
            m.call(a, -1, 2, { eb: e.qd, tg: { lb: Zb }, Dc: r }, b, { lb: 1 }, { lb: -2 });
            a.cc = b
        }

        function nc(o, n) {
            var b = this, e, g, h, k, c;
            m.call(b, -1e8, 2e8, { Ic: 100 });
            b.Hc = function () {
                M = d;
                R = j;
                f.k(i.gg, t(w.V()), w.V())
            };
            b.Gc = function () {
                M = l;
                k = l;
                var a = w.sf();
                f.k(i.hg, t(w.V()), w.V());
                !a.lb && Dc(a.bf, s)
            };
            b.Nb = function (i, f) {
                var b;
                if (k) b = c;
                else {
                    b = g;
                    if (h) {
                        var d = f / h;
                        b = a.Bc(d) * (g - e) + e
                    }
                }
                w.u(b)
            };
            b.Fb = function (a, d, c, f) {
                e = a;
                g = d;
                h = c;
                w.u(a);
                b.u(0);
                b.wc(c, f)
            };
            b.xe = function (a) {
                k = d;
                c = a;
                b.vc(a, j, d)
            };
            b.we = function (a) { c = a };
            w = new Bc;
            w.zb(o);
            w.zb(n)
        }

        function pc() {
            var c = this, a = Xb();
            b.s(a, 0);
            b.L(a, "pointerEvents", "none");
            c.H = a;
            c.Eb = function () {
                b.T(a);
                b.Sb(a)
            }
        }

        function xc(o, g) {
            var e = this, q, L, v, k, y = [], x, B, W, G, Q, F, h, w, p;
            m.call(e, -u, u + 1, {});

            function E(a) {
                q && q.yc();
                T(o, a, 0);
                F = d;
                q = new I.G(o, I, b.Yf(b.j(o, "idle")) || lc);
                q.u(0)
            }

            function Z() { q.ec < I.ec && E() }

            function M(p, r, o) {
                if (!G) {
                    G = d;
                    if (k && o) {
                        var h = o.width, c = o.height, n = h, m = c;
                        if (h && c && a.ob) {
                            if (a.ob & 3 && (!(a.ob & 4) || h > K || c > J)) {
                                var j = l, q = K / J * c / h;
                                if (a.ob & 1) j = q > 1;
                                else if (a.ob & 2) j = q < 1;
                                n = j ? h * J / c : K;
                                m = j ? J : c * K / h
                            }
                            b.l(k, n);
                            b.m(k, m);
                            b.C(k, (J - m) / 2);
                            b.E(k, (K - n) / 2)
                        }
                        b.A(k, "absolute");
                        f.k(i.Re, g)
                    }
                }
                b.T(r);
                p && p(e)
            }

            function Y(b, c, d, f) {
                if (f == R && s == g && N)
                    if (!Cc) {
                        var a = t(b);
                        A.Ef(a, g, c, e, d);
                        c.Qe();
                        U.Mb(a - U.gc() - 1);
                        U.u(a);
                        z.Fb(b, b, 0)
                    }
            }

            function cb(b) {
                if (b == R && s == g) {
                    if (!h) {
                        var a = j;
                        if (A)
                            if (A.cb == g) a = A.If();
                            else A.Eb();
                        Z();
                        h = new vc(o, g, a, q);
                        h.Nc(p)
                    }
                    !h.zc() && h.ic()
                }
            }

            function S(d, f, l) {
                if (d == g) {
                    if (d != f) C[f] && C[f].Le();
                    else !l && h && h.Ke();
                    p && p.Yc();
                    var m = R = b.P();
                    e.gb(b.K(j, cb, m))
                } else {
                    var k = c.min(g, d), i = c.max(g, d), o = c.min(i - k, k + r - i), n = u + a.Je - 1;
                    (!Q || o <= n) && e.gb()
                }
            }

            function db() {
                if (s == g && h) {
                    h.db();
                    p && p.Id();
                    p && p.Hd();
                    h.tc()
                }
            }

            function eb() { s == g && h && h.db() }

            function ab(a) { !P && f.k(i.Yd, g, a) }

            function O() {
                p = w.pInstance;
                h && h.Nc(p)
            }

            e.gb = function (c, a) {
                a = a || v;
                if (y.length && !G) {
                    b.O(a);
                    if (!W) {
                        W = d;
                        f.k(i.Xd, g);
                        b.f(y, function (a) {
                            if (!b.v(a, "src")) {
                                a.src = b.j(a, "src2");
                                b.U(a, a["display-origin"])
                            }
                        })
                    }
                    b.Ze(y, k, b.K(j, M, c, a))
                } else M(c, a)
            };
            e.Rd = function () {
                var h = g;
                if (a.Wc < 0) h -= r;
                var d = h + a.Wc * tc;
                if (D & 2) d = t(d);
                if (!(D & 1)) d = c.max(0, c.min(d, r - u));
                if (d != g) {
                    if (A) {
                        var e = A.wf(r);
                        if (e) {
                            var i = R = b.P(), f = C[t(d)];
                            return f.gb(b.K(j, Y, d, f, e, i), v)
                        }
                    }
                    bb(d)
                }
            };
            e.sc = function () { S(g, g, d) };
            e.Le = function () {
                p && p.Id();
                p && p.Hd();
                e.Oc();
                h && h.Qf();
                h = j;
                E()
            };
            e.Qe = function () { b.T(o) };
            e.Oc = function () { b.O(o) };
            e.Nf = function () { p && p.Yc() };

            function T(a, c, e) {
                if (b.v(a, "jssor-slider")) return;
                if (!F) {
                    if (a.tagName == "IMG") {
                        y.push(a);
                        if (!b.v(a, "src")) {
                            Q = d;
                            a["display-origin"] = b.U(a);
                            b.T(a)
                        }
                    }
                    b.od() && b.s(a, (b.s(a) || 0) + 1)
                }
                var f = b.xb(a);
                b.f(f, function (f) {
                    var h = f.tagName, i = b.j(f, "u");
                    if (i == "player" && !w) {
                        w = f;
                        if (w.pInstance) O();
                        else b.a(w, "dataavailable", O)
                    }
                    if (i == "caption") {
                        if (c) {
                            b.uc(f, b.j(f, "to"));
                            b.xf(f, b.j(f, "bf"));
                            b.j(f, "3d") && b.yf(f, "preserve-3d")
                        } else if (!b.kd()) {
                            var g = b.pb(f, l, d);
                            b.Db(g, f, a);
                            b.Cb(f, a);
                            f = g;
                            c = d
                        }
                    } else if (!F && !e && !k) {
                        if (h == "A") {
                            if (b.j(f, "u") == "image") k = b.ef(f, "IMG");
                            else k = b.q(f, "image", d);
                            if (k) {
                                x = f;
                                b.U(x, "block");
                                b.N(x, V);
                                B = b.pb(x, d);
                                b.A(x, "relative");
                                b.oc(B, 0);
                                b.L(B, "backgroundColor", "#000")
                            }
                        } else if (h == "IMG" && b.j(f, "u") == "image") k = f;
                        if (k) {
                            k.border = 0;
                            b.N(k, V)
                        }
                    }
                    T(f, c, e + 1)
                })
            }

            e.hc = function (c, b) {
                var a = u - b;
                Zb(L, a)
            };
            e.cb = g;
            n.call(e);
            b.zf(o, b.j(o, "p"));
            b.Af(o, b.j(o, "po"));
            var H = b.q(o, "thumb", d);
            if (H) {
                b.pb(H);
                b.T(H)
            }
            b.O(o);
            v = b.pb(fb);
            b.s(v, 1e3);
            b.a(o, "click", ab);
            E(d);
            e.Ff = k;
            e.Fc = B;
            e.cc = L = o;
            b.I(L, v);
            f.ub(203, S);
            f.ub(28, eb);
            f.ub(24, db)
        }

        function vc(y, g, p, q) {
            var a = this, n = 0, u = 0, h, j, e, c, k, t, r, o = C[g];
            m.call(a, 0, 0);

            function v() {
                b.Sb(L);
                cc && k && o.Fc && b.I(L, o.Fc);
                b.O(L, !k && o.Ff)
            }

            function w() { a.ic() }

            function x(b) {
                r = b;
                a.db();
                a.ic()
            }

            a.ic = function () {
                var b = a.nb();
                if (!B && !M && !r && s == g) {
                    if (!b) {
                        if (h && !k) {
                            k = d;
                            a.tc(d);
                            f.k(i.ig, g, n, u, h, c)
                        }
                        v()
                    }
                    var l, p = i.Cc;
                    if (b != c)
                        if (b == e) l = c;
                        else if (b == j) l = e;
                        else if (!b) l = j;
                        else l = a.xc();
                    f.k(p, g, b, n, j, e, c);
                    var m = N && (!E || F);
                    if (b == c) (e != c && !(E & 12) || m) && o.Rd();
                    else (m || b != e) && a.wc(l, w)
                }
            };
            a.Ke = function () { e == c && e == a.nb() && a.u(j) };
            a.Qf = function () {
                A && A.cb == g && A.Eb();
                var b = a.nb();
                b < c && f.k(i.Cc, g, -b - 1, n, j, e, c)
            };
            a.tc = function (a) { p && b.Hb(jb, a && p.bc.zg ? "" : "hidden") };
            a.hc = function (b, a) {
                if (k && a >= h) {
                    k = l;
                    v();
                    o.Oc();
                    A.Eb();
                    f.k(i.fg, g, n, u, h, c)
                }
                f.k(i.Jf, g, a, n, j, e, c)
            };
            a.Nc = function (a) {
                if (a && !t) {
                    t = a;
                    a.ub($JssorPlayer$.rf, x)
                }
            };
            p && a.fc(p);
            h = a.Kb();
            a.fc(q);
            j = h + q.Ac;
            e = h + q.Qc;
            c = a.Kb()
        }

        function Kb(a, c, d) {
            b.E(a, c);
            b.C(a, d)
        }

        function Zb(c, b) {
            var a = x > 0 ? x : eb, d = zb * b * (a & 1), e = Ab * b * (a >> 1 & 1);
            Kb(c, d, e)
        }

        function Pb() {
            pb = M;
            Ib = z.xc();
            G = w.V()
        }

        function gc() {
            Pb();
            if (B || !F && E & 12) {
                z.db();
                f.k(i.kf)
            }
        }

        function ec(f) {
            if (!B && (F || !(E & 12)) && !z.zc()) {
                var d = w.V(), b = c.ceil(G);
                if (f && c.abs(H) >= a.Rc) {
                    b = c.ceil(d);
                    b += hb
                }
                if (!(D & 1)) b = c.min(r - u, c.max(b, 0));
                var e = c.abs(b - d);
                e = 1 - c.pow(1 - e, 5);
                if (!P && pb) z.re(Ib);
                else if (d == b) {
                    sb.Nf();
                    sb.sc()
                } else z.Fb(d, b, e * Vb)
            }
        }

        function Hb(a) { !b.j(b.Zb(a), "nodrag") && b.Jb(a) }

        function rc(a) { Yb(a, 1) }

        function Yb(a, c) {
            a = b.td(a);
            var k = b.Zb(a);
            if (!O && !b.j(k, "nodrag") && sc() && (!c || a.touches.length == 1)) {
                B = d;
                yb = l;
                R = j;
                b.a(g, c ? "touchmove" : "mousemove", Bb);
                b.P();
                P = 0;
                gc();
                if (!pb) x = 0;
                if (c) {
                    var h = a.touches[0];
                    ub = h.clientX;
                    vb = h.clientY
                } else {
                    var e = b.Ec(a);
                    ub = e.x;
                    vb = e.y
                }
                H = 0;
                gb = 0;
                hb = 0;
                f.k(i.jf, t(G), G, a)
            }
        }

        function Bb(e) {
            if (B) {
                e = b.td(e);
                var f;
                if (e.type != "mousemove") {
                    var l = e.touches[0];
                    f = { x: l.clientX, y: l.clientY }
                } else f = b.Ec(e);
                if (f) {
                    var j = f.x - ub, k = f.y - vb;
                    if (c.floor(G) != G) x = x || eb & O;
                    if ((j || k) && !x) {
                        if (O == 3)
                            if (c.abs(k) > c.abs(j)) x = 2;
                            else x = 1;
                        else x = O;
                        if (mb && x == 1 && c.abs(k) - c.abs(j) > 3) yb = d
                    }
                    if (x) {
                        var a = k, i = Ab;
                        if (x == 1) {
                            a = j;
                            i = zb
                        }
                        if (!(D & 1)) {
                            if (a > 0) {
                                var g = i * s, h = a - g;
                                if (h > 0) a = g + c.sqrt(h) * 5
                            }
                            if (a < 0) {
                                var g = i * (r - u - s), h = -a - g;
                                if (h > 0) a = -g - c.sqrt(h) * 5
                            }
                        }
                        if (H - gb < -2) hb = 0;
                        else if (H - gb > 2) hb = -1;
                        gb = H;
                        H = a;
                        rb = G - H / i / (Y || 1);
                        if (H && x && !yb) {
                            b.Jb(e);
                            if (!M) z.xe(rb);
                            else z.we(rb)
                        }
                    }
                }
            }
        }

        function ab() {
            qc();
            if (B) {
                B = l;
                b.P();
                b.J(g, "mousemove", Bb);
                b.J(g, "touchmove", Bb);
                P = H;
                z.db();
                var a = w.V();
                f.k(i.hf, t(a), a, t(G), G);
                E & 12 && Pb();
                ec(d)
            }
        }

        function jc(c) {
            if (P) {
                b.Gf(c);
                var a = b.Zb(c);
                while (a && v !== a) {
                    a.tagName == "A" && b.Jb(c);
                    try {
                        a = a.parentNode
                    } catch (d) {
                        break
                    }
                }
            }
        }

        function Jb(a) {
            C[s];
            s = t(a);
            sb = C[s];
            Ub(a);
            return s
        }

        function Dc(a, b) {
            x = 0;
            Jb(a);
            f.k(i.ff, t(a), b)
        }

        function Ub(a, c) {
            wb = a;
            b.f(S, function (b) { b.Qb(t(a), a, c) })
        }

        function sc() {
            var b = i.Zc || 0, a = X;
            if (mb) a & 1 && (a &= 1);
            i.Zc |= a;
            return O = a & ~b
        }

        function qc() {
            if (O) {
                i.Zc &= ~X;
                O = 0
            }
        }

        function Xb() {
            var a = b.wb();
            b.N(a, V);
            b.A(a, "absolute");
            return a
        }

        function t(a) { return (a % r + r) % r }

        function kc(b, d) {
            if (d)
                if (!D) {
                    b = c.min(c.max(b + wb, 0), r - u);
                    d = l
                } else if (D & 2) {
                    b = t(b + wb);
                    d = l
                }
            bb(b, a.Ib, d)
        }

        function xb() { b.f(S, function (a) { a.Rb(a.Ob.ug <= F) }) }

        function hc() {
            if (!F) {
                F = 1;
                xb();
                if (!B) {
                    E & 12 && ec();
                    E & 3 && C[s].sc()
                }
            }
        }

        function Ec() {
            if (F) {
                F = 0;
                xb();
                B || !(E & 12) || gc()
            }
        }

        function ic() {
            V = { jb: K, kb: J, g: 0, i: 0 };
            b.f(T, function (a) {
                b.N(a, V);
                b.A(a, "absolute");
                b.Hb(a, "hidden");
                b.T(a)
            });
            b.N(fb, V)
        }

        function ob(b, a) { bb(b, a, d) }

        function bb(g, f, j) {
            if (Rb && (!B && (F || !(E & 12)) || a.Mc)) {
                M = d;
                B = l;
                z.db();
                if (f == k) f = Vb;
                var e = Cb.nb(), b = g;
                if (j) {
                    b = e + g;
                    if (g > 0) b = c.ceil(b);
                    else b = c.floor(b)
                }
                if (D & 2) b = t(b);
                if (!(D & 1)) b = c.max(0, c.min(b, r - u));
                var i = (b - e) % r;
                b = e + i;
                var h = e == b ? 0 : f * c.abs(i);
                h = c.min(h, f * u * 1.5);
                z.Fb(e, b, h || 1)
            }
        }

        f.vc = function () {
            if (!N) {
                N = d;
                C[s] && C[s].sc()
            }
        };

        function W() { return b.l(y || p) }

        function lb() { return b.m(y || p) }

        f.W = W;
        f.X = lb;

        function Eb(c, d) {
            if (c == k) return b.l(p);
            if (!y) {
                var a = b.wb(g);
                b.Kc(a, b.Kc(p));
                b.sb(a, b.sb(p));
                b.U(a, "block");
                b.A(a, "relative");
                b.C(a, 0);
                b.E(a, 0);
                b.Hb(a, "visible");
                y = b.wb(g);
                b.A(y, "absolute");
                b.C(y, 0);
                b.E(y, 0);
                b.l(y, b.l(p));
                b.m(y, b.m(p));
                b.uc(y, "0 0");
                b.I(y, a);
                var h = b.xb(p);
                b.I(p, y);
                b.L(p, "backgroundImage", "");
                b.f(h, function (c) {
                    b.I(b.j(c, "noscale") ? p : a, c);
                    b.j(c, "autocenter") && Mb.push(c)
                })
            }
            Y = c / (d ? b.m : b.l)(y);
            b.Bf(y, Y);
            var f = d ? Y * W() : c, e = d ? c : Y * lb();
            b.l(p, f);
            b.m(p, e);
            b.f(Mb, function (a) {
                var c = b.qc(b.j(a, "autocenter"));
                b.bg(a, c)
            })
        }

        f.af = Eb;
        n.call(f);
        f.H = p = b.qb(p);
        var a = b.M({ ob: 0, Je: 1, Xb: 1, Tb: 0, Vb: l, Lb: 1, hb: d, Mc: d, Wc: 1, ed: 3e3, dd: 1, Ib: 500, Bc: e.vd, Rc: 20, cd: 0, rb: 1, bd: 0, nf: 1, Ub: 1, nd: 1 }, fc);
        a.hb = a.hb && b.vf();
        if (a.of != k) a.ed = a.of;
        if (a.pf != k) a.bd = a.pf;
        var eb = a.Ub & 3, tc = (a.Ub & 4) / -4 || 1, kb = a.vg, I = b.M({ G: q, hb: a.hb }, a.mg);
        I.vb = I.vb || I.yg;
        var Fb = a.kg, Z = a.lg, db = a.xg, Q = !a.nf, y, v = b.q(p, "slides", Q), fb = b.q(p, "loading", Q) || b.wb(g), Nb = b.q(p, "navigator", Q), dc = b.q(p, "arrowleft", Q), ac = b.q(p, "arrowright", Q), Lb = b.q(p, "thumbnavigator", Q), oc = b.l(v), mc = b.m(v), V, T = [], uc = b.xb(v);
        b.f(uc, function (a) {
            if (a.tagName == "DIV" && !b.j(a, "u")) T.push(a);
            else b.od() && b.s(a, (b.s(a) || 0) + 1)
        });
        var s = -1, wb, sb, r = T.length, K = a.rg || oc, J = a.sg || mc, Wb = a.cd, zb = K + Wb, Ab = J + Wb, bc = eb & 1 ? zb : Ab, u = c.min(a.rb, r), jb, x, O, yb, S = [], Qb, Sb, Ob, cc, Cc, N, E = a.dd, lc = a.ed, Vb = a.Ib, qb, tb, ib, Rb = u < r, D = Rb ? a.Lb : 0, X, P, F = 1, M, B, R, ub = 0, vb = 0, H, gb, hb, Cb, w, U, z, Tb = new pc, Y, Mb = [];
        if (r) {
            if (a.hb) Kb = function (a, c, d) { b.Pb(a, { fb: c, bb: d }) };
            N = a.Vb;
            f.Ob = fc;
            ic();
            b.v(p, "jssor-slider", d);
            b.s(v, b.s(v) || 0);
            b.A(v, "absolute");
            jb = b.pb(v, d);
            b.Db(jb, v);
            if (kb) {
                cc = kb.wg;
                qb = kb.G;
                tb = u == 1 && r > 1 && qb && (!b.kd() || b.sd() >= 8)
            }
            ib = tb || u >= r || !(D & 1) ? 0 : a.bd;
            X = (u > 1 || ib ? eb : -1) & a.nd;
            var Gb = v, C = [], A, L, Db = b.Hf(), mb = Db.tf, G, pb, Ib, rb;
            Db.jd && b.L(Gb, Db.jd, ([j, "pan-y", "pan-x", "none"])[X] || "");
            U = new zc;
            if (tb) A = new qb(Tb, K, J, kb, mb);
            b.I(jb, U.cc);
            b.Hb(v, "hidden");
            L = Xb();
            b.L(L, "backgroundColor", "#000");
            b.oc(L, 0);
            b.Db(L, Gb.firstChild, Gb);
            for (var cb = 0; cb < T.length; cb++) {
                var wc = T[cb], yc = new xc(wc, cb);
                C.push(yc)
            }
            b.T(fb);
            Cb = new Ac;
            z = new nc(Cb, U);
            if (X) {
                b.a(v, "mousedown", Yb);
                b.a(v, "touchstart", rc);
                b.a(v, "dragstart", Hb);
                b.a(v, "selectstart", Hb);
                b.a(g, "mouseup", ab);
                b.a(g, "touchend", ab);
                b.a(g, "touchcancel", ab);
                b.a(h, "blur", ab)
            }
            E &= mb ? 10 : 5;
            if (Nb && Fb) {
                Qb = new Fb.G(Nb, Fb, W(), lb());
                S.push(Qb)
            }
            if (Z && dc && ac) {
                Z.Lb = D;
                Z.rb = u;
                Sb = new Z.G(dc, ac, Z, W(), lb());
                S.push(Sb)
            }
            if (Lb && db) {
                db.Tb = a.Tb;
                Ob = new db.G(Lb, db);
                S.push(Ob)
            }
            b.f(S, function (a) {
                a.jc(r, C, fb);
                a.ub(o.ac, kc)
            });
            b.L(p, "visibility", "visible");
            Eb(W());
            b.a(v, "click", jc, d);
            b.a(p, "mouseout", b.Wb(hc, p));
            b.a(p, "mouseover", b.Wb(Ec, p));
            xb();
            a.Xb && b.a(g, "keydown", function (b) {
                if (b.keyCode == 37) ob(-a.Xb);
                else b.keyCode == 39 && ob(a.Xb)
            });
            var nb = a.Tb;
            if (!(D & 1)) nb = c.max(0, c.min(nb, r - u));
            z.Fb(nb, nb, 0)
        }
    };
    i.Yd = 21;
    i.jf = 22;
    i.hf = 23;
    i.gg = 24;
    i.hg = 25;
    i.Xd = 26;
    i.Re = 27;
    i.kf = 28;
    i.cf = 202;
    i.ff = 203;
    i.ig = 206;
    i.fg = 207;
    i.Jf = 208;
    i.Cc = 209;
    var o = { ac: 1 },
        r = function (e, C) {
            var f = this;
            n.call(f);
            e = b.qb(e);
            var s, A, z, r, k = 0, a, m, i, w, x, h, g, q, p, B = [], y = [];

            function v(a) { a != -1 && y[a].Ge(a == k) }

            function t(a) { f.k(o.ac, a * m) }

            f.H = e;
            f.Qb = function (a) {
                if (a != r) {
                    var d = k, b = c.floor(a / m);
                    k = b;
                    r = a;
                    v(d);
                    v(b)
                }
            };
            f.Rb = function (a) { b.O(e, a) };
            var u;
            f.jc = function (D) {
                if (!u) {
                    s = c.ceil(D / m);
                    k = 0;
                    var o = q + w, r = p + x, n = c.ceil(s / i) - 1;
                    A = q + o * (!h ? n : i - 1);
                    z = p + r * (h ? n : i - 1);
                    b.l(e, A);
                    b.m(e, z);
                    for (var f = 0; f < s; f++) {
                        var C = b.mf();
                        b.Lc(C, f + 1);
                        var l = b.lf(g, "numbertemplate", C, d);
                        b.A(l, "absolute");
                        var v = f % (n + 1);
                        b.E(l, !h ? o * v : f % i * o);
                        b.C(l, h ? r * v : c.floor(f / (n + 1)) * r);
                        b.I(e, l);
                        B[f] = l;
                        a.dc & 1 && b.a(l, "click", b.K(j, t, f));
                        a.dc & 2 && b.a(l, "mouseover", b.Wb(b.K(j, t, f), l));
                        y[f] = b.rc(l)
                    }
                    u = d
                }
            };
            f.Ob = a = b.M({ gd: 10, fd: 10, hd: 1, dc: 1 }, C);
            g = b.q(e, "prototype");
            q = b.l(g);
            p = b.m(g);
            b.Cb(g, e);
            m = a.id || 1;
            i = a.qf || 1;
            w = a.gd;
            x = a.fd;
            h = a.hd - 1;
            a.nc == l && b.v(e, "noscale", d);
            a.tb && b.v(e, "autocenter", a.tb)
        },
        s = function (a, g, h) {
            var c = this;
            n.call(c);
            var r, q, e, f, i;
            b.l(a);
            b.m(a);

            function k(a) { c.k(o.ac, a, d) }

            function p(c) {
                b.O(a, c || !h.Lb && e == 0);
                b.O(g, c || !h.Lb && e >= q - h.rb);
                r = c
            }

            c.Qb = function (b, a, c) {
                if (c) e = a;
                else {
                    e = b;
                    p(r)
                }
            };
            c.Rb = p;
            var m;
            c.jc = function (c) {
                q = c;
                e = 0;
                if (!m) {
                    b.a(a, "click", b.K(j, k, -i));
                    b.a(g, "click", b.K(j, k, i));
                    b.rc(a);
                    b.rc(g);
                    m = d
                }
            };
            c.Ob = f = b.M({ id: 1 }, h);
            i = f.id;
            if (f.nc == l) {
                b.v(a, "noscale", d);
                b.v(g, "noscale", d)
            }
            if (f.tb) {
                b.v(a, "autocenter", f.tb);
                b.v(g, "autocenter", f.tb)
            }
        };

    function q(e, d, c) {
        var a = this;
        m.call(a, 0, c);
        a.yc = b.Pc;
        a.Ac = 0;
        a.Qc = c
    }

    var t = function (n, g, l) {
        var a = this, o, h = {}, i = g.vb, c = new m(0, 0);
        m.call(a, 0, 0);

        function j(d, c) {
            var a = {};
            b.f(d, function (d, f) {
                var e = h[f];
                if (e) {
                    if (b.Kf(d)) d = j(d, c || f == "e");
                    else if (c) if (b.Yb(d)) d = o[d];
                    a[e] = d
                }
            });
            return a
        }

        function k(e, c) {
            var a = [], d = b.xb(e);
            b.f(d, function (d) {
                var h = b.j(d, "u") == "caption";
                if (h) {
                    var e = b.j(d, "t"), g = i[b.qc(e)] || i[e], f = { H: d, bc: g };
                    a.push(f)
                }
                if (c < 5) a = a.concat(k(d, c + 1))
            });
            return a
        }

        function r(d, e, a) {
            b.f(e, function (g) {
                var e = j(g), f = b.Sc(e.eb);
                if (e.i) {
                    e.F = e.i;
                    f.F = f.i;
                    delete e.i
                }
                if (e.g) {
                    e.B = e.g;
                    f.B = f.g;
                    delete e.g
                }
                var i = { eb: f, W: a.jb, X: a.kb }, h = new m(g.b, g.d, i, d, a, e);
                c.zb(h);
                a = b.Ee(a, e)
            });
            return a
        }

        function q(a) {
            b.f(a, function (d) {
                var a = d.H, f = b.l(a), e = b.m(a), c = { i: b.E(a), g: b.C(a), F: 0, B: 0, ib: 1, mb: b.s(a) || 0, ab: 0, R: 0, S: 0, p: 1, n: 1, fb: 0, bb: 0, Q: 0, Ab: 0, Bb: 0, jb: f, kb: e, c: { g: 0, z: f, D: e, i: 0 } };
                c.Vc = c.i;
                c.Uc = c.g;
                r(a, d.bc, c)
            })
        }

        function t(g, f, h) {
            var e = g.b - f;
            if (e) {
                var b = new m(f, e);
                b.zb(c, d);
                b.Mb(h);
                a.zb(b)
            }
            a.dg(g.d);
            return e
        }

        function s(f) {
            var d = c.gc(), e = 0;
            b.f(f, function (c, f) {
                c = b.M({ d: l }, c);
                t(c, d, e);
                d = c.b;
                e += c.d;
                if (!f || c.t == 2) {
                    a.Ac = d;
                    a.Qc = d + c.d
                }
            })
        }

        a.yc = function () { a.u(-1, d) };
        o = [f.Ad, f.zd, f.xd, f.Dd, f.Od, f.Nd, f.be, f.ge, f.fe, f.Vd, f.Ud, f.Td, f.ld, f.Sd, f.Qd, f.Pd, f.ee, f.ae, f.Md, f.Cd, f.Fd, f.Jd, f.Xe, f.ag, f.Zf, f.Xf, f.Wf, f.Vf, f.Ye, f.Uf, f.Rf, f.Pf, f.Mf, f.Lf, f.Sf, f.cg, f.eg];
        var u = { g: "y", i: "x", D: "m", z: "t", ab: "r", R: "rX", S: "rY", p: "sX", n: "sY", fb: "tX", bb: "tY", Q: "tZ", Ab: "kX", Bb: "kY", ib: "o", eb: "e", mb: "i", c: "c" };
        b.f(u, function (b, a) { h[b] = a });
        q(k(n, 1));
        c.u(-1);
        var p = g.Bg || [], e = [].concat(p[b.qc(b.j(n, "b"))] || []);
        e.push({ b: c.Kb(), d: e.length ? 0 : l });
        s(e);
        a.u(-1)
    };
    jssor_1_slider_init = function () {
        var g = [[{ b: 5500, d: 3e3, o: -1, r: 240, e: { r: 2 } }], [{ b: -1, d: 1, o: -1 }, { b: 4e3, d: 1500, y: 180, o: 1, e: { y: 16 } }], [{ b: -1, d: 1, o: -1 }, { b: 1e3, d: 1e3, x: -85, y: -40, o: 1 }], [{ b: -1, d: 1, o: -1 }, { b: 2e3, d: 1e3, x: -77, y: -52, o: 1 }], [{ b: -1, d: 1, o: -1 }, { b: 3e3, d: 1e3, x: -85, y: -45, o: 1 }], [{ b: -1, d: 1, o: -1 }, { b: 0, d: 1e3, o: 1 }], [{ b: -1, d: 1, o: -1, r: -150 }, { b: 7500, d: 1600, o: 1, r: 150, e: { r: 3 } }], [{ b: 1e4, d: 2e3, x: -379, e: { x: 7 } }], [{ b: 1e4, d: 2e3, x: -379, e: { x: 7 } }], [{ b: -1, d: 1, o: -1, r: 288, sX: 9, sY: 9 }, { b: 9100, d: 900, x: -1400, y: -660, o: 1, r: -288, sX: -9, sY: -9, e: { r: 6 } }, { b: 1e4, d: 1600, x: -200, o: -1, e: { x: 16 } }], [{ b: -1, d: 1, o: -1 }, { b: 0, d: 1e3, o: 1 }, { b: 4e3, d: 1e3, y: -90 }], [{ b: -1, d: 1, o: -1 }, { b: 1e3, d: 1e3, o: 1 }, { b: 4500, d: 1e3, y: -95 }], [{ b: -1, d: 1, o: -1 }, { b: 2e3, d: 2e3, x: -640, o: 1, e: { o: 7 } }], [{ b: -1, d: 1, o: -1 }, { b: 5500, d: 1500, o: 1 }], [{ b: 500, d: 1e3, x: 431 }, { b: 2500, d: 1500, x: 432 }], [{ b: 700, d: 1e3, x: 395 }, { b: 2700, d: 1500, x: 395 }], [{ b: 4700, d: 1e3, x: 454 }, { b: 6600, d: 1500, x: 454 }], [{ b: 4900, d: 1e3, x: 395 }, { b: 6800, d: 1500, x: 395 }], [{ b: 9100, d: 1e3, x: 435 }], [{ b: 9300, d: 1e3, x: 395 }]], j = { Vb: d, Ib: 800, Bc: f.ld, mg: { G: t, vb: g }, lg: { G: s }, kg: { G: r } }, e = new i("jssor_1", j);

        function k() {
            var d = b.df(e.H, "slides");
            if (d) {
                var c = d[1];
                if (c) {
                    var a = b.q(c, "ad");
                    if (!a) {
                        a = b.wb();
                        b.sb(a, "position:absolute;top:0px;right:0px;width:80px;height:20px;background-color:rgba(255,255,140,0.5);font-size: 12px;line-height: 20px;text-align:center;");
                        b.Lc(a, "Jssor Slider");
                        b.I(c, a)
                    }
                }
            }
        }

        k();

        function a() {
            var b = e.H.parentNode.clientWidth;
            if (b) {
                b = c.min(b, 1920);
                e.af(b)
            } else h.setTimeout(a, 30)
        }

        a();
        b.a(h, "load", a);
        b.a(h, "resize", a);
        b.a(h, "orientationchange", a)
    }
})(window, document, Math, null, true, false)

jssor_1_slider_init();